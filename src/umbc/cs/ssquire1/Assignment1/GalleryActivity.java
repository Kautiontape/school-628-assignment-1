package umbc.cs.ssquire1.Assignment1;

import java.io.File;
import java.util.ArrayList;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class GalleryActivity extends Activity implements OnItemClickListener {
	private static final String PREFS_NAME = "628Project1Prefs";
	private String SAVE_FOLDER;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			SAVE_FOLDER = extras.getString("IMAGE_LOCATION");
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("saveFolder", SAVE_FOLDER);
			editor.commit();
		} else {
			SAVE_FOLDER = settings.getString("saveFolder", "");			
		}
		
		File directory = new File(SAVE_FOLDER);
		File[] farr = directory.listFiles();
		if(farr != null) {
			setContentView(R.layout.activity_gallery);
			
			ArrayList<String> files = new ArrayList<String>();
			for(File f : farr) {
				files.add(f.getName());
			}
			
			ListView fileList = (ListView)findViewById(R.id.gallery_list);
			ArrayAdapter<String> fileListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, files);
			fileList.setAdapter(fileListAdapter);
			fileList.setOnItemClickListener(this);
		} else {
			setContentView(R.layout.activity_gallery_empty);
		}		
	}

	@Override
	public void onItemClick(AdapterView<?> l, View v, int position, long id) {
		String file = (String)l.getItemAtPosition(position);

		Intent image = new Intent(getBaseContext(), ImagePreview.class);
		image.putExtra("IMAGE_LOCATION", SAVE_FOLDER + file);
		startActivity(image);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gallery, menu);
		return true;
	}

	// http://developer.android.com/training/implementing-navigation/ancestral.html
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	            // This is called when the Home (Up) button is pressed in the Action Bar.
	            Intent parentActivityIntent = new Intent(this, MainActivity.class);
	            parentActivityIntent.addFlags(
	                    Intent.FLAG_ACTIVITY_CLEAR_TOP |
	                    Intent.FLAG_ACTIVITY_NEW_TASK);
	            startActivity(parentActivityIntent);
	            finish();
	            return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

}
