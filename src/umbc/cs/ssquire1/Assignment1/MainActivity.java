package umbc.cs.ssquire1.Assignment1;

import java.io.File;
import java.io.IOException;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private static final String PREFS_NAME = "628Project1Prefs";
	private static final int REQUEST_TAKE_PICTURE = 1818;
	private static final String TAG = "621Assignment";
	
	SharedPreferences settings;

	private Button takePictureButton, viewPicturesButton;
	private ImageView imagePreview;
	
	String SAVE_LOCATION = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SAVE_LOCATION = getApplicationContext().getExternalCacheDir().getAbsolutePath() + File.separatorChar + getString(R.string.save_folder) + File.separatorChar;
		
		takePictureButton = (Button)findViewById(R.id.takepicbutton);
		viewPicturesButton = (Button)findViewById(R.id.viewpicsbutton);
		imagePreview = (ImageView)findViewById(R.id.imagepreview);
		
		takePictureButton.setOnClickListener(this);
		viewPicturesButton.setOnClickListener(this);
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		loadLastPicture();
	}
	
	private void takePicture() {
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {			
			Uri lastPhotoUri = getImageUri();
			Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, lastPhotoUri);
			startActivityForResult(cameraIntent, REQUEST_TAKE_PICTURE);

			SharedPreferences.Editor editor = settings.edit();
			editor.putString("lastUri", lastPhotoUri.getPath());
			editor.commit();
		} else {
			new AlertDialog.Builder(getApplicationContext()).setMessage(getString(R.string.media_not_mounted)).create().show();
		}
	}
	
	private void retrievePicture(Intent data) {
		String lastUriString = settings.getString("lastUri", null);
		File f = new File(lastUriString);
		long size = f.length();
		
		if(size > 0) {
			Toast.makeText(getApplicationContext(), getString(R.string.picture_taken_toast), Toast.LENGTH_SHORT).show();
			loadLastPicture();
		} else {
			Toast.makeText(getApplicationContext(), getString(R.string.picture_not_taken_toast), Toast.LENGTH_SHORT).show();
			deleteLastPicture();
		}
	}
	
	private void loadLastPicture() {
		String lastUriString = settings.getString("lastUri", null);
		if(lastUriString != null) {
			Log.i(TAG, lastUriString);
			Bitmap bm = BitmapFactory.decodeFile(lastUriString);
			imagePreview.setImageBitmap(bm);
		} else {
			// no last picture
		}
	}
	
	private void deleteLastPicture() {
		String lastUriString = settings.getString("lastUri", null);
		if(lastUriString != null) {
			File f = new File(lastUriString);
			f.delete();
		}
	}
	
	private Uri getImageUri() {
		// http://stackoverflow.com/a/5054673
		String fileName = System.currentTimeMillis() + ".jpg";
		String saveLoc = SAVE_LOCATION + fileName;
		
		File f = new File(saveLoc);
		if(!f.exists()) {
			f.getParentFile().mkdirs();
			try {
				f.createNewFile();
			} catch (IOException e) {
				Log.e(TAG, "Error saving file", e);
			}
		}
		Log.i(TAG, saveLoc);
		
		return Uri.fromFile(f);
	}

	@Override
	public void onClick(View arg0) {
		if(arg0.getId() == takePictureButton.getId()) {
			this.takePicture();
		} else if(arg0.getId() == viewPicturesButton.getId()) {
			Intent gallery = new Intent(this, GalleryActivity.class);
			gallery.putExtra("IMAGE_LOCATION", SAVE_LOCATION);
			startActivity(gallery);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(TAG, "onActivityResult called: " + requestCode + "-" + resultCode);
		if(requestCode == REQUEST_TAKE_PICTURE) {
			retrievePicture(data);
		}
	
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
